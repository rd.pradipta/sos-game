import React from 'react';

const Alert = props => {
	let type = "alert alert-";
  type += props.type + " " + props.style_addition
	
	return (
		<div className={type} role="alert">
			{props.content}
		</div>
	);
};

export default Alert;