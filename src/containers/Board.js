import React, { Component } from 'react';
import Square from '../components/Square';

class Board extends Component{
	
	createSquare = (i) => {
		return(
			<Square
				value={this.props.filled[i]}
				click={() => this.props.onClick(i)}
				key={i}
			/>
		);
  }
	
  createBoard = () => {
    let table = []
		var index = 0
    for (let i = 0; i < 6; i++) {
      let child = []
      for (let j = 0; j < 6; j++) {
				child.push(this.createSquare(index))
				index++
      }
      table.push(<div className="board-row" key={i}>{child}</div>)
    }
    return table
  }

	render(){
		return(
			<div>
				{this.createBoard()}
			</div>
		);
  }
}

export default Board;