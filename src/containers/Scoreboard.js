import React, { Component } from 'react';

import Alert from '../components/Alert';
import Button from '../components/Button';

class Scoreboard extends Component{
  constructor(props){
    super(props);
    this.state = {
			showResetConfirmation : false,
		};
	}

  // Fungsi untuk mengganti state untuk menampilkan konfirmasi reset
  askReset = () => {
    this.setState({
      showResetConfirmation : true
    });
  }

  // Fungsi untuk melakukan render
  render(){
    let reset_box;
    let empty_line = <br></br>
    if(this.state.showResetConfirmation){
      reset_box = 
        <div>
          <div className="text-left">
            <h5 className="font-weight-bold">Apakah Anda yakin ingin memulai dari awal?</h5>
            <p> Pastikan hal ini telah dipikirkan oleh kedua pemain untuk mencegah terjadinya
                pertikaian dan baku hantam antarpemain.
            </p>
          </div>
          <div className="btn-group" role="group">
            <Button
              onClick={() => {
                this.setState({
                  showResetConfirmation : false
                })
                this.props.reset()
              }}
              type={"dark"}
              title={"Ya"}
            />
            <Button
              onClick={() => {
                this.setState({
                  showResetConfirmation : false
                });
              }}
              type={"danger"}
              title={"Tidak"}
            />
          </div>
        </div>
    }
    else reset_box = null;
    return(
      <div className="col-md-6">
        <div className="card shadow">
          <div className="card-body text-center">
            <h3>Papan Skor SOS</h3>
            <div className="card">
              <div className="card-header">
                <div className="row">
                  <div className="col">
                    <h5>{this.props.first_player}</h5>
                    <h4>{this.props.first_point}</h4>
                  </div>
                  <div className="col">
                    <h5>{this.props.second_player}</h5>
                    <h4>{this.props.second_point}</h4>
                  </div>
                </div>
              </div>
            </div>
            {empty_line} 
            <Alert
              type={"primary"}
              content={this.props.message}
              style_addition={"text-center"}
            />            
            <Button
              onClick={() => this.askReset()}
              type={"dark"}
              title={"Ulangi Permainan"}
            />
            <p></p>
            {reset_box}
          </div>
        </div>
      </div>
    );
  }
}

export default Scoreboard