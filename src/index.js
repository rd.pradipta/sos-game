import React from 'react';
import ReactDOM from 'react-dom';
import SOS from './App';
import './assets/index.css';

ReactDOM.render(
  <SOS />,
  document.getElementById('root')
);